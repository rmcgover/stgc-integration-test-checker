# B180 sTGC Integration Quick Test Checker

Test data is located at `/eos/atlas/atlascerngroupdisk/det-nsw-stgc/stgc-integration-test-checker-data`

This software is available to `stgcic` at `/afs/cern.ch/user/s/stgcic/public/stgc-integration-test-checker`

The git repo associated with the software is at <https://gitlab.cern.ch/rmcgover/stgc-integration-test-checker/>
