# -*- coding: utf-8 -*-
from __future__ import (division, absolute_import, print_function,
                        unicode_literals)

import json
import os.path
import sys

from PyQt4.QtCore import QEvent, QRegExp, Qt
from PyQt4.QtGui import (QApplication, QCheckBox, QLabel, QMainWindow, QPlainTextEdit, QPushButton, QTableWidget, QTableWidgetItem, QWidget, QFormLayout, QHBoxLayout, QVBoxLayout)

from src.detailed_dialog import exceptionWarning
from src.unicodecsv import UnicodeReader as csv_reader

EOSDIR = os.path.join('/eos/atlas/atlascerngroupdisk/det-nsw-stgc',
                      'stgc-integration-test-checker-data')

with open('testspec.json') as testspec:
    TESTS = json.load(testspec)


def test_error_message(test):
    name = test['name']
    contact = test['contact']
    if contact:
        m, n = contact.find('(') + 1, contact.find(')')
        if m != -1 and n != -1:
            address = contact[m:n]
            query = 'subject={}&body={}'.format(
                'Test checker {}'.format(name),
                'Copy and paste data from "Show Details..." here'
            )
            mailto = "<a href='mailto:{}?{}'>{}</a>".format(
                address, query, address)
            contact = contact[:m] + mailto + contact[n:]
    msg = 'Error reading test "{}"'.format(name)
    if contact:
        msg += '<br/>Contact: {}'.format(contact)
    return msg


class Result(object):
    PASS = 'PASS'
    FAIL = 'FAIL'
    MISSING = 'MISSING'


def normalize(result):
    return result.upper()


class ResultAccumulator(object):
    def __init__(self):
        self.result = Result.PASS

    def add(self, result):
        """Truth table:
            P M F
          --------
        P | P M F
        M | M M F
        F | F F F
        """
        if result == Result.FAIL:
            self.result = result
        elif result == Result.MISSING:
            if self.result != Result.FAIL:
                self.result = result


class BoardEntry(QPlainTextEdit):
    BOARD_RE = QRegExp(
        '|'.join((
            r'.*-.*-(.*)-.*\t20MNE(?:SFE6|SFE8|PFEB)\d{5}',
            r'(\d{5})'
        ))
    )

    def __init__(self, *args, **kwargs):
        super(QPlainTextEdit, self).__init__(*args, **kwargs)
        self.installEventFilter(self)

    def eventFilter(self, obj, event):
        if (obj is self and event.type() == QEvent.KeyPress
                and event.key() == Qt.Key_Tab):
            s = self.toPlainText()
            c = self.textCursor()
            n = c.position()
            s1, s2 = s[:n], s[n:]
            self.setPlainText(s1 + '\t' + s2)
            c.setPosition(n + 1)
            self.setTextCursor(c)
            event.accept()
            return True
        return super(QPlainTextEdit, self).eventFilter(obj, event)

    def boards(self, validate=False):
        ids = set()
        # no list comprehension because QRegularExpression is Qt 5 only :c
        for board in self.toPlainText().split('\n'):
            if not board:
                continue
            if not self.BOARD_RE.exactMatch(board):
                if validate:
                    raise ValueError('"{}" does not specify a board'
                                     .format(board))
                continue
            _, id1, id2 = self.BOARD_RE.capturedTexts()
            ids.add(unicode(id1 or id2))
        if validate:
            self.setPlainText('\n'.join(sorted(ids)) + ('\n' if ids else ''))
        return ids


class Checker(QWidget):
    def __init__(self, parent=None):
        super(QWidget, self).__init__(parent)
        layout = QVBoxLayout(self)
        self.setLayout(layout)
        top_layout = QHBoxLayout()
        layout.addLayout(top_layout)
        boards_layout = QVBoxLayout()
        top_layout.addLayout(boards_layout)
        self.boards = BoardEntry(self)
        boards_layout.addWidget(self.boards)
        self.boards.textChanged.connect(self.on_boards_changed)
        self.boardcount = QLabel('Boards: 0')
        boards_layout.addWidget(self.boardcount)
        check_b = QPushButton('Check', self)
        boards_layout.addWidget(check_b)
        check_b.clicked.connect(self.check)
        tests_layout = QFormLayout()
        top_layout.addLayout(tests_layout)
        self.tests = {}
        for test in TESTS:
            cb = QCheckBox(self)
            self.tests[test['name']] = cb
            tests_layout.addRow(test['name'], cb)
        self.results = QTableWidget(0, 0, self)
        layout.addWidget(self.results)

    def check(self):
        try:
            boards = self.boards.boards(validate=True)
        except ValueError:
            exceptionWarning(self, 'Error', 'Bad board specification')
            return
        if not boards:
            return
        tests = tuple(test for test in TESTS
                      if self.tests[test['name']].isChecked())
        if not tests:
            return
        results = {}
        for test in tests:
            name = test['name']
            try:
                results[name] = self.get_test_results(name, boards)
            except (ValueError, IOError, OSError):
                exceptionWarning(self, 'Error',
                                 test_error_message(test))
                results[name] = {board: Result.MISSING for board in boards}
        self.display_results(results, boards)

    @staticmethod
    def get_test_results(test, board_ids):
        path = os.path.join(EOSDIR, test + '.csv')
        with open(path, str('rb')) as testfile:
            reader = csv_reader(testfile)
            try:
                # allow blank lines, trailing comma
                sca_ids, results = zip(*(row[:2] for row in reader if row))
            except ValueError:
                raise ValueError('Malformed or empty file "{}"'.format(path))
        if len(set(sca_ids)) != len(sca_ids):
            raise ValueError('Duplicate entry in "{}"'.format(path))
        if any(normalize(result) not in (Result.PASS, Result.FAIL)
               for result in results):
            raise ValueError('Test result not "PASS" or "FAIL" in "{}"'
                             .format(path))
        test_result = {sca_id: normalize(result)
                       for sca_id, result in zip(sca_ids, results)
                       if sca_id in board_ids}
        test_result.update({sca_id: Result.MISSING
                            for sca_id in board_ids
                            if sca_id not in sca_ids})
        return test_result

    COLORS = {
        Result.PASS: Qt.green,
        Result.MISSING: Qt.yellow,
        Result.FAIL: Qt.red
    }

    def display_results(self, test_results, boards):
        boards = sorted(boards)
        self.results.clear()
        self.results.setEnabled(True)
        tests = [test['name'] for test in TESTS
                 if test['name'] in test_results]
        self.results.setColumnCount(len(tests) + 1)
        self.results.setHorizontalHeaderLabels(['SCA ID'] + tests)
        self.results.setRowCount(len(boards))
        for r in range(len(boards)):
            board = boards[r]
            board_result = ResultAccumulator()
            self.results.setItem(r, 0, QTableWidgetItem(board))
            for c in range(len(tests)):
                result = test_results[tests[c]][board]
                board_result.add(result)
                item = QTableWidgetItem(result)
                item.setBackground(self.COLORS[result])
                self.results.setItem(r, c + 1, item)
            self.results.item(r, 0).setBackground(
                self.COLORS[board_result.result])

    def on_boards_changed(self):
        count = len(self.boards.boards())
        self.boardcount.setText('Boards: {}'.format(count))
        self.results.setEnabled(False)


if __name__ == '__main__':
    def debug():
        global EOSDIR, TESTS
        EOSDIR = 'test/'
        TESTS = [
            {'name': 'valid',
             'contact': 'Bobby McGovern (robert.mcgovern@cern.ch)'},
            {'name': 'valid2',
             'contact': 'Bobby McGovern (robert.mcgovern@cern.ch)'},
            {'name': 'noresult',
             'contact': 'Bobby McGovern (robert.mcgovern@cern.ch)'},
            {'name': 'duplicate',
             'contact': 'Bobby McGovern (robert.mcgovern@cern.ch)'},
            {'name': 'badresult',
             'contact': 'Bobby McGovern (robert.mcgovern@cern.ch)'},
            {'name': 'nofile',
             'contact': 'Bobby McGovern (robert.mcgovern@cern.ch)'},
            {'name': 'emptyfile',
             'contact': 'Bobby McGovern (robert.mcgovern@cern.ch)'},
        ]

    def main():
        if '--debug' in sys.argv:
            debug()
        app = QApplication(sys.argv)
        window = QMainWindow()
        checker = Checker(window)
        window.setCentralWidget(checker)
        window.show()
        app.exec_()

    main()
