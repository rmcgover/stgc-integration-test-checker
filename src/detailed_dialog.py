# -*- coding: utf-8 -*-
"""Convenience methods to emulate QMessageBox static functions with
informative and detailed text.
Updated: 2020-06-02 (for python 2)
"""

import sys
import traceback

from PyQt4.QtCore import Qt
from PyQt4.QtGui import QMessageBox


### Base function

def detailedDialog(
        parent,
        icon,
        title, text, informativetext, detailedtext,
        buttons, defaultbutton
):
    """Open a QMessageBox with the given parameters
    Arguments:
        parent -- the Qt parent
        icon -- a value from the QMesssageBox Icon enum
        title -- the dialog title
        text -- the message box text
        informativetext -- the message box informative text (below text)
        detailedtext -- the message box detailed text (hidden in dropdown)
        buttons -- or'd values from the QMessageBox StandardButtons enum
        defaultbutton -- a value from the QMessageBox StandardButtons enum
    """
    m = QMessageBox(parent)
    m.setIcon(icon)
    m.setWindowTitle(title)
    m.setTextFormat(Qt.RichText)
    m.setText(text)
    m.setInformativeText(informativetext)
    m.setDetailedText(detailedtext)
    m.setStandardButtons(buttons)
    m.setDefaultButton(defaultbutton)
    return m.exec_()


### Dialog templates

def detailedInformation(
        parent,
        title, text, informativetext, detailedtext,
        buttons=QMessageBox.Ok, defaultbutton=QMessageBox.NoButton
):
    """Open an information message box
    Arguments:
        parent -- the Qt parent
        title -- the dialog title
        text -- the message box text
        informativetext -- the message box informative text (below text)
        detailedtext -- the message box detailed text (hidden in dropdown)
        buttons -- or'd values from the QMessageBox StandardButtons enum
        defaultbutton -- a value from the QMessageBox StandardButtons enum
    """
    return detailedDialog(
        parent,
        QMessageBox.Information,
        title, text, informativetext, detailedtext,
        buttons, defaultbutton
    )


def detailedWarning(
        parent,
        title, text, informativetext, detailedtext,
        buttons=QMessageBox.Ok, defaultbutton=QMessageBox.NoButton
):
    """Open a warning message box
    Arguments:
        parent -- the Qt parent
        title -- the dialog title
        text -- the message box text
        informativetext -- the message box informative text (below text)
        detailedtext -- the message box detailed text (hidden in dropdown)
        buttons -- or'd values from the QMessageBox StandardButtons enum
        defaultbutton -- a value from the QMessageBox StandardButtons enum
    """
    return detailedDialog(
        parent,
        QMessageBox.Warning,
        title, text, informativetext, detailedtext,
        buttons, defaultbutton
    )


def detailedCritical(
        parent,
        title, text, informativetext, detailedtext,
        buttons=QMessageBox.Ok, defaultbutton=QMessageBox.NoButton
):
    """Open a critical message box
    Arguments:
        parent -- the Qt parent
        title -- the dialog title
        text -- the message box text
        informativetext -- the message box informative text (below text)
        detailedtext -- the message box detailed text (hidden in dropdown)
        buttons -- or'd values from the QMessageBox StandardButtons enum
        defaultbutton -- a value from the QMessageBox StandardButtons enum
    """
    return detailedDialog(
        parent,
        QMessageBox.Critical,
        title, text, informativetext, detailedtext,
        buttons, defaultbutton
    )


def detailedQuestion(
        parent,
        title, text, informativetext, detailedtext,
        buttons=(QMessageBox.Yes | QMessageBox.No),
        defaultbutton=QMessageBox.NoButton
):
    """Open a question message box
    Arguments:
        parent -- the Qt parent
        title -- the dialog title
        text -- the message box text
        informativetext -- the message box informative text (below text)
        detailedtext -- the message box detailed text (hidden in dropdown)
        buttons -- or'd values from the QMessageBox StandardButtons enum
        defaultbutton -- a value from the QMessageBox StandardButtons enum
    """
    return detailedDialog(
        parent,
        QMessageBox.Question,
        title, text, informativetext, detailedtext,
        buttons, defaultbutton
    )


### Exception dialog helpers

def format_text(text, width=80):
    """Pads `text` to `width` characters"""
    return '\n'.join(line.ljust(width) for line in text.split('\n'))


def format_exc_only():
    """Provides the results of traceback.format_exception_only, with the
    interface of traceback.format_exc
    """
    etype, value, _ = sys.exc_info()
    return ''.join(traceback.format_exception_only(etype, value))


### Exception dialog templates

def exceptionWarning(
        parent,
        title, text, informativetext=None, detailedtext=None,
        buttons=QMessageBox.Ok, defaultbutton=QMessageBox.NoButton
):
    """Opens a warning message box, with informative text containing the
    exception and detailed text containing the exception traceback.
    """
    text = format_text(text)
    informativetext = informativetext or format_exc_only()
    detailedtext = detailedtext or traceback.format_exc()
    return detailedDialog(
        parent,
        QMessageBox.Warning,
        title, text, informativetext, detailedtext,
        buttons, defaultbutton
    )


def exceptionCritical(
        parent,
        title, text, informativetext=None, detailedtext=None,
        buttons=QMessageBox.Ok, defaultbutton=QMessageBox.NoButton
):
    """Opens a critical message box, with informative text containing the
    exception and detailed text containing the exception traceback.
    """
    text = format_text(text)
    informativetext = informativetext or format_exc_only()
    detailedtext = detailedtext or traceback.format_exc()
    return detailedDialog(
        parent,
        QMessageBox.Critical,
        title, text, informativetext, detailedtext,
        buttons, defaultbutton
    )
